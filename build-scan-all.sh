# VERSION="v15.2.0"
VERSION="master"
REPO="registry.gitlab.com/gitlab-org/build/cng/"
TAG_SUFFIX="-fips"

IMAGES=$(cat <<EOF
alpine-certificates
cfssl-self-sign
gitaly
gitlab-base
gitlab-container-registry
gitlab-exporter
gitlab-geo-logcursor
gitlab-kas
gitlab-mailroom
gitlab-pages
gitlab-rails-ee
gitlab-shell
gitlab-sidekiq-ee
gitlab-toolbox-ee
gitlab-webservice-ee
gitlab-workhorse-ee
kubectl
EOF
)

main() {
  mkdir -p ./results

  for tag in $IMAGES; do
    image="${REPO}$tag:${VERSION}${TAG_SUFFIX}"
    outfile="./results/$tag${TAG_SUFFIX}.json"

    scan "$image" "$outfile"

    dockerfile="./dockerfiles/$tag.Dockerfile"
    if [ -f "$dockerfile" ]; then
      distroless_image="${REPO}$tag:${VERSION}-chainguard"
      build "$dockerfile" "$distroless_image"
      scan "$distroless_image" "./results/$tag-chainguard.json"
    fi
  done

  format > data.yaml
  to_csv "data.yaml" > data.csv
}

scan() {
  image=$1
  outfile=$2

  # trivy image --format template --template "@cve-summary.tmpl" --output "$outfile" --scanners vuln "$image"
  trivy image --format json --output "$outfile" --scanners vuln "$image"
}


format() {
  yq eval-all '. as $item ireduce([]; . + ({ "image": $item.ArtifactName, "vulnerabilities": ($item.Results | map_values(.Vulnerabilities // []) | flatten | map_values(.Severity) | group_by(.) | map({ "key": .[0], "value": (. | length) }) | from_entries ) }))' ./results/*.json 
}

to_csv() {
  echo "image,low,med,high,crit"
  yq 'map([ .image, (.vulnerabilities.LOW // 0), (.vulnerabilities.MEDIUM // 0), (.vulnerabilities.HIGH // 0), (.vulnerabilities.CRITICAL // 0) ]) | @csv' "$1"
}

build() {
  docker build . -f "$1" -t "$2"
}

main;