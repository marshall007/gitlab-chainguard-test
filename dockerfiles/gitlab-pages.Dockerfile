FROM cgr.dev/chainguard/go:1.19-dev as build

ADD https://gitlab.com/gitlab-org/gitlab-pages/-/archive/master/gitlab-pages-master.tar.gz /gitlab-pages.tar.gz

RUN mkdir -p /build && \
    tar -xvf /gitlab-pages.tar.gz -C /build && \
    cd /build/gitlab-pages-master && \
    make

FROM cgr.dev/chainguard/wolfi-base:latest

COPY --from=build /build/gitlab-pages-master/gitlab-pages /bin/gitlab-pages

ENTRYPOINT [ "/bin/gitlab-pages" ]
