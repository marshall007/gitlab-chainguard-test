FROM cgr.dev/chainguard/go:1.19-dev as build

ADD https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/archive/master/gitlab-agent-master.tar.gz /gitlab-agent.tar.gz

RUN mkdir -p /build && \
    tar -xvf /gitlab-agent.tar.gz -C /build && \
    cd /build/gitlab-agent-master && \
    make kas

FROM cgr.dev/chainguard/wolfi-base:latest

COPY --from=build /build/gitlab-agent-master/kas /bin/kas

ENTRYPOINT [ "/bin/kas" ]
